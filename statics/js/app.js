/**
 * Created by bruce on 3/09/2015.
 */
define(function(require){
    'use strict';
    var $ = require('jQuery'),
        themes = require('js/ui/themes'),
        pages = require('js/pages/pages'),
        aboutMe = require('js/pages/about_me'),
        portfolio = require('js/pages/portfolio'),
        contactMe = require('js/pages/contact_me');


    function init(){
        themes.init();
        pages.init();
        aboutMe.init();
        portfolio.init();
        contactMe.init();
        $('body').addClass('loaded');
    }

    return {init: init};
});