/**
 * Created by bruce on 6/09/2015.
 */
define(['jQuery','underscore','js/data/data_source'],function($, _, data){
    'use strict';
    //var data = <%- data %>;
    var dom = {
        categories: '.skill-categories',
        category: '.category',
        skills: '.skills .skill'
    };

    var dataPortfolio;

    function init(){
        dataPortfolio = data.getPortfolioData();
        $(dom.categories).on('click', dom.category, filterByCategory);

    }

    function filterByCategory(e){
        //debugger;
        $(dom.categories+' '+dom.category).find('.btn').removeClass('active');
        var $this = $(e.currentTarget);
        $this.find('.btn').addClass('active');
        var className = $this.attr('class').toLowerCase();
        if(className.indexOf('category-all') !== -1){
            showAllSkills();
        }else{
            showCategorySkills(className);
        }
    }

    function showAllSkills(){
        $(dom.skills).fadeIn();
    }
    function hideAllSkills(){
        $(dom.skills).fadeOut();
    }

    function showCategorySkills(className){
        //debugger;
        var showSelectors = [],
            hideSelectors = [];

        _.forEach(dataPortfolio.categories, function(category){
            category = category.toLowerCase();
            if(className.indexOf('category-'+category) !== -1){
                _.forEach(dataPortfolio.skills, function(skill){
                    if(_.findIndex(skill.categories, function(c){
                            return c.toLowerCase() === category;
                        }) !== -1){
                        showSelectors.push('.skill-'+skill.name);
                    } else{
                        hideSelectors.push('.skill-'+skill.name);
                    }
                });
            }
        });

        $(hideSelectors.join(',')).fadeOut(500);

        setTimeout(function(){
            $(showSelectors.join(',')).show();
        }, 500);
    }



    return {
        init: init
    };
});