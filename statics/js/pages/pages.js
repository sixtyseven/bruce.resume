/**
 * Created by bruce on 20/07/2015.
 */

define(['jQuery'], function($){
    'use strict';
    var PREFIX_PAGE = 'js-page-',
        CLASS_PAGE = '.page';

    //dom variables
    var dom = {
        navLinks:     '.js-nav-links',
        currentLink:  '.js-nav-links .current',
        currentPage:  '.page.current',
        newPage:       ''
    };

    function showPage(e){
        e.preventDefault();
        e.stopImmediatePropagation();

        var $this = $(e.currentTarget);
        if($this.hasClass('current') || $(".page:animated")[0]){
            return false;
        }
        var idLi = $this.attr('data-id');
        dom.newPage = '.' + PREFIX_PAGE + idLi + CLASS_PAGE;
        $(dom.currentPage).fadeOut(400, "linear", function(){
            $(dom.newPage).fadeIn(400, "linear", function(){
                $(dom.currentLink).removeClass('current');
                $(dom.currentPage).removeClass('current');
                $(dom.newPage).addClass('current');
                $this.addClass('current');
            });
        });
    }

    function initCurrentPage(){
        $('.js-btn-about-me').addClass('current');
        $('.js-page-about-me.page').addClass('current');
        $('.pages .page').hide();
        $('.page.current').show();
    }

    function init(){
        initCurrentPage()
        var $navLinks = $(dom.navLinks);
        $navLinks.on('click','li', showPage);
    }

    return {
        init: init
    };
});

