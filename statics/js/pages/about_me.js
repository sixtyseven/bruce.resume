define(['jQuery', 'js/libs/jqbar'], function($, jqbar){
    'use strict';
    var setting = {
        barWidth: 13,
        barColor: '#21ba82'
    };

    function init(){
        $('.jqbar.jqbar-html').jqbar({ label: 'HTML', value: 85, barColor: setting.barColor, barWidth:setting.barWidth });
        $('.jqbar.jqbar-css').jqbar({ label: 'CSS', value: 90, barColor: setting.barColor, barWidth:setting.barWidth });
        $('.jqbar.jqbar-js').jqbar({ label: 'Javascript', value: 85, barColor: setting.barColor, barWidth:setting.barWidth });
        $('.jqbar.jqbar-nodejs').jqbar({ label: 'NodeJS', value: 50, barColor: setting.barColor, barWidth:setting.barWidth });

        $('.bar-contents .bar-content').hide();
        $('.bar-contents .bar-content.active').show();
        $('.jqbar').on('click', jqBarClick);
    }

    function jqBarClick(e){

        $('.jqbar').removeClass('active');
        var contentClass = $(e.currentTarget).addClass('active').attr('data-id').replace('jqbar','content');
        $('.bar-contents .bar-content').css({'opacity':'0',display:'none'});
        $('.bar-contents' + ' .' + contentClass).css('display','block').animate({opacity:1},1000);
    }

    return {
        init: init
    };
});