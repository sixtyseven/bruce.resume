define(['jQuery', 'underscore'], function($, _){
    'use strict';

    function init(){
        $('.contact-me-form').on('submit', sendMessage);

        $('#client-name').keyup(function(){
            validateField('client-name', 'required');
        });
        $('#client-email').keyup(function(){
            validateField('client-email', 'email');
        });
        $('#client-budget').change(function(){
            validateField('client-budget', 'required');
        });
    }

    function sendMessage(e){
        e.preventDefault();
        if(validateContactMeForm()){
            var $form = $(e.currentTarget);
            var $status = $form.find('.send-message .status');
            $status.text('sending message...');

            $.get('/contact_me', $form.serializeArray(), function(data){
                if(data && data.sent){
                    $status.text('Thank you for contacting us. We will try to reach you ASAP.');
                }else{
                    $status.text('Unable to process, please try again later.');
                }
            });
        }
    }

    function validateContactMeForm(){
        var validated = true;
        if(!validateField('client-email', 'required')){
            validated = false;
        }
        if(!validateField('client-email', 'email')){
            validated = false;
        }
        if(!validateField('client-name', 'required')){
            validated = false;
        }
        if(!validateField('client-budget', 'required')){
            validated = false;
        }

        return validated;
    }

    function validateField(fieldId, fieldType){
        var element = document.getElementById(fieldId),
            value,
            result = true;

        if(element){
            value = element.value;
        }

        switch (fieldType){
            case 'required':
                result = validateRequiredField(element, value);
                break;
            case 'email':
                result = validateEmailField(element, value);
                break;
            default:
                break;
        }

        return result;
    }

    function validateRequiredField(textElement, textValue){
        var result = true;
        if (textElement && !_validateRequired(textValue)){
            result = false;
            _addErrorIcon(textElement);
        }
        else if(textElement){
            _addSuccessIcon(textElement);
        }

        return result;
    }


    function validateEmailField(emailElement, emailValue){
        var result = true;

        if(emailElement){
            emailValue = emailElement.value;
        }

        if(emailElement && !_validateEmail(emailValue)){
            result = false;
            _addErrorIcon(emailElement);
        }
        else if(emailElement){
            _addSuccessIcon(emailElement);
        }

        return result;
    }

    function _validateRequired(input){
        var result = (input !== null && input !== "");
        return result;
    }

    function _addErrorIcon(element){
        var classes;
        var id_attr = element.getAttribute("id") + "_v_icon";
        var closestFormGroupElement = __findClosestClass(element, ".form-group");
        classes = closestFormGroupElement.className.replace("has-success","") + " has-error";
        closestFormGroupElement.className = classes;
        if(document.getElementById(id_attr)){
            classes = document.getElementById(id_attr).className.replace("glyphicon-ok","") + " glyphicon-remove";
            document.getElementById(id_attr).className = classes;
        }
    }

    function _addSuccessIcon(element){
        var idAttr = '#' + element.id + "_v_icon";
        var $closestFormGroupElement = $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        $(idAttr).removeClass('glyphicon-remove').addClass('glyphicon-ok');
    }

    function _addErrorMessage(element, error_message){
        var classes;
        var id_attr = element.id + "_em";
        document.getElementById(id_attr).innerHTML = error_message;
        if(document.getElementById(id_attr)){
            classes = document.getElementById(id_attr).className.replace("invisible","");
            document.getElementById(id_attr).className = classes;
        }
    }
    function _removeErrorMessage(element){
        var id_attr = element.id + "_em";
        var classes = document.getElementById(id_attr).className + " invisible";
        if(document.getElementById(id_attr)){
            document.getElementById(id_attr).className = classes;
        }
    }


    function _validateEmail(email){
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
    function _validateRequired(input){
        return (input !== null && input !== "");
    }


    /**
     * @param element: the current element
     * @param cName: the class name which ans element should contain
     * @return first ancestor(start form itself) who has the cName. If not find, return null.
     */
    function __findClosestClass(element, cName){
        //start from element itself
        var parent = element;
        while(parent!==document.body){
            if(parent && __hasClass(parent, cName)){
                return parent;
            }else{
                parent = parent.parentNode;
            }
        }
        return null;
    }

    /**
     * @param element: the current element
     * @param cName: the class name which current element has (or does not  has)
     * @return boolean
     */
    function __hasClass(element, cName) {
        var i;
        var classes = element.className.split(" ");
        var classNameI;
        for (i=0; i<classes.length; i++){
            classNameI = "."+classes[i];
            if (classNameI === __trim(cName) || classes[i]===__trim(cName)){
                return true;
            }
        }

        return false;
    }

    // for IE browsers versions <= IE 8
    function __trim(str){
        return str.replace(/^\s+|\s+$/g, '');
    }

    return  {
        init: init
    };


});
