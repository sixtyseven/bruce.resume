/**
 * Created by bruce on 18/07/2015.
 */

require.config({
    baseUrl: '/',
    paths: {
        'jQuery': 'bower_components/jquery/dist/jquery',
        'underscore': 'bower_components/underscore/underscore',
        'bootstrap': 'bower_components/bootstrap/dist/js/bootstrap',
        'modernizr': 'bower_components/modernizr/modernizr'
    },
    shim: {
        'jQuery':{
            exports: '$'
        },
        'underscore':{
            exports: '_'
        },
        'bootstrap':{
            'deps': ['jQuery']
        }
    }
});


require(['jQuery', 'js/app',], function($,  app){
    'use strict';
    $(function(){
        console.log('main js');
        app.init();

    });

});