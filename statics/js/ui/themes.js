/**
 * Created by bruce on 19/07/2015.
 */


define(['jQuery', 'js/utils/components/cookies'], function($, cookies){
    'use strict';

    var settings = {
        cookieName: 'i18next'
    };

   //dom cache
    var dom = {
        btnEn : '.js-btn-en',
        btnCn : '.js-btn-cn',
        imgEn : '.js-img-en',
        imgCn : '.js-img-cn'
    };

    var $btnEn,
        $btnCn,
        $imgEn,
        $imgCn;

    return {
        init: function(){
            $btnEn = $(dom.btnEn);
            $btnCn = $(dom.btnCn);
            $imgEn = $(dom.imgEn);
            $imgCn = $(dom.imgCn);

            $btnEn.on('click', function(){
                changeTheme.apply(this, ['en']);
            });
            $btnCn.on('click', function(){
                changeTheme.apply(this, ['cn']);
            });
            loadThemeFromCookie();
        }
    };

    function changeTheme(locale){
        var currentLocale = cookies.readCookie(settings.cookieName);
        if(currentLocale === locale){
            return;
        }
        cookies.createCookie(settings.cookieName, locale, 30);

        //location.search = '?setLng='+ locale;

        location.reload();
    }

    function loadThemeFromCookie(){
        var currentLocale = cookies.readCookie(settings.cookieName) || 'en';
        loadTheme(currentLocale);
    }

    function loadTheme(locale){
        changeStyle(locale);
    }

    function changeStyle(locale){

        if(locale === 'cn'){
            $('body').addClass('theme theme-cn');
            $btnCn.parent().children().removeClass('active');
            $btnCn.addClass('active');
            $imgCn.parent().children().hide();
            $imgCn.show();

        }else{ //default
            $('body').removeClass('theme theme-cn');
            $btnEn.parent().children().removeClass('active');
            $btnEn.addClass('active');
            $imgEn.parent().children().hide();
            $imgEn.show();
        }
    }

});