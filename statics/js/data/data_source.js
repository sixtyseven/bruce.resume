/**
 * Created by bruce on 10/09/2015.
 * my is a global variable (object)
 */

define(['jQuery'], function($){
    'use strict';


    function getPortfolioData(){
        return my.data.portfolio;
    }

    return{
        getPortfolioData: getPortfolioData
    };
});
