/**
 * Created by bruce on 18/07/2015.
 * manupulate cookies in the browser
 */

define(['jQuery', 'underscore'], function($, _){
    'use strict';
   return  {
       createCookie: function(name,value,days) {
           var expires = "";
           if(days){
               var date = new Date();
               date.setTime(date.getTime() + days*24*60*60*1000);
               expires = "expires="+date.toGMTString();
           }

           document.cookie = name+"="+value+";"+expires+"; path=/";
       },
       readCookie: function(name) {
           var ca = document.cookie.split(';');

           var find = _.find(ca,function(c){
               return name === $.trim(c.split('=')[0]);
           });

           return find ? find.split('=')[1] : null;
       },
       eraseCookie: function(name) {
           this.createCookie(name,"",-1);
       }
   };
});

