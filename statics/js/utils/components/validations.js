/**
 * Created by bruce on 14/09/2015.
 */


define(['jQuery', 'underscore'], function($, _){
    'use strict';

    var dom = {
        required: 'data-required',
        validationType: 'data-validation',
        validationMsg: 'data-msg'
    };




    function validateEmail(email) {
        var regExp = /^[\w\.\+-]{1,}\@([\da-zA-Z-]{1,}\.){1,}[\da-zA-Z-]{2,6}$/;
        if (!regExp.test(email)) { return false; }
        return true;
    }



    return  {

    };
});


