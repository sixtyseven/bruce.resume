/**
 * Created by bruce on 11/09/2015.
 */
var app = require('./helpers/app');

var should = require('should'),
    supertest = require('supertest');

describe('portfolio data', function(){
   it('should return valid portfolio data', function(done){
       supertest(app)
           .get('/data')
           .expect(200)
           .end(function(err, res){
              res.status.should.equal(200);
               done();
           });
   });
    it('should have skills', function(done){
        supertest(app)
            .get('/data')
            .expect(200)
            .end(function(err, res){
                res.body.portfolio.skills.length.should.greaterThan(0);
                done();
            });
    });

    it('should have categories', function(done){
        supertest(app)
            .get('/data')
            .expect(200)
            .end(function(err, res){
                res.body.portfolio.categories.length.should.greaterThan(0);
                done();
            });
    });



});