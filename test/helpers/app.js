var app = require('../../app.js'),
    dataSource = require('../data_source');

module.exports = app(dataSource);