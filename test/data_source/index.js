/**
 * Created by bruce on 3/09/2015.
 */


exports.data = {
    portfolio : {
        categories: ['javascript', 'tools', 'css', 'library', 'design', 'framework'],
        skills:[
            {
                name: 'gruntjs',
                description: 'The Javascript task runner. Automation, performing repetitive tasks like minification, compilation, unit testing and linting.',
                category: ['tools', 'javascript']
            },
            {
                name: 'vagrant',
                description: 'Create and configure lightweight, reproducible, and portable development environments.',
                category: ['tools']
            },
            //{
            //    name: 'docker',
            //    description: 'Docker is an open platform for developers and sysadmins to build, ship, and run distributed applications, whether on laptops, data center VMs, or the cloud.',
            //    category: ['tools']
            //},
            {
                name: 'bootstrap',
                description: 'A sleek, intuitive, and powerful mobile first front-end framework for faster and easier web development.',
                category: ['library', 'css']
            },
            {
                name: 'favicon',
                description: 'A favicon /\ˈfævɪkɒn/ (short for Favorite icon), also known as a shortcut icon, Web site icon, tab icon or bookmark icon, is a file containing one or more small icons, most commonly 16×16 pixels, associated with a particular website or web page.',
                category: ['design']
            },
            {
                name: 'i18next',
                description: 'I18next is a full-featured i18n javascript library for translating your webapplication. runs in browser, under node.js, rhino and other javascript runtimes.',
                category: ['library', 'javascript']
            },
            {
                name: 'jquery',
                description: 'jQuery is a fast, small, and feature-rich JavaScript library. It makes things like HTML document traversal and manipulation, event handling, animation, and Ajax much simpler with an easy-to-use API that works across a multitude of browsers.',
                category: ['library', 'javascript']
            },
            {
                name: 'underscore',
                description: 'Underscore is a JavaScript library that provides a whole mess of useful functional programming helpers without extending any built-in objects.',
                category: ['library', 'javascript']
            },
            {
                name: 'requirejs',
                description: 'RequireJS is a JavaScript file and module loader. It is optimized for in-browser use, but it can be used in other JavaScript environments, like Rhino and Node.',
                category: ['library', 'javascript', 'tools']
            },
            {
                name: 'knockoutjs',
                description: 'Knockout is a standalone JavaScript implementation of the Model-View-ViewModel pattern with templates.',
                category: ['library', 'Javascript']
            },
            {
                name: 'expressjs',
                description: 'Fast, unopinionated, minimalist web framework for Node.js.',
                category: ['framework', 'Javascript']
            }
        ]
    }
};
