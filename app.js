module.exports = function(dataSource){

    var express = require('express');
    var path = require('path');
    var favicon = require('serve-favicon');
    var logger = require('morgan');
    var cookieParser = require('cookie-parser');
    var bodyParser = require('body-parser');
    var compression = require('compression');
    var i18n = require('i18next');
    var routesIndex = require('./routes/index')(dataSource);

    var app = express();

    i18n.init({
        ignoreRoutes: ['./statics'],
        ns: { namespaces: ['common'], defaultNs:'common' }
    });


// view engine setup
    app.set('views', path.join(__dirname, 'views'));
    app.set('view engine', 'ejs');



// configuration
// set up the middleware
    app.use(cookieParser());
    app.use(favicon(__dirname + '/statics/public/favicon.ico'));
    app.use(logger('dev'));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(i18n.handle);
    app.use(compression());
    app.use(express.static(path.join(__dirname, 'statics')));
    app.use('/', routesIndex);



// catch 404 and forward to error handler
    app.use(function(req, res, next) {
        var err = new Error('Not Found');
        err.status = 404;
        next(err);
    });



// development error handler
// will print stacktrace
    if (app.get('env') === 'development') {
        app.use(function(err, req, res, next) {
            res.status(err.status || 500);
            res.render('error', {
                message: err.message,
                error: err
            });
        });
    }

// production error handler
// no stacktraces leaked to user
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message
        });
    });

    i18n.registerAppHelper(app);

    return app;


};

