module.exports = function(grunt){
    require('time-grunt')(grunt);
    require('grunt-spritesmith')(grunt);
    require('load-grunt-config')(grunt);
};