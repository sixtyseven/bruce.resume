/**
 * Created by bruce on 18/07/2015.
 */
module.exports = {
    bsFiles: {
        src: ['./statics/css/styles.css']
    }
    ,
    options: {
        watchTask: true,
        proxy: 'v.ubuntu.trusty:3000',
        ui: {
            port: 3050
        }
    }
}

