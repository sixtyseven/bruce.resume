/**
 * Created by bruce on 8/08/2015.
 */

module.exports = {
    dist: {
        options:{
            baseUrl: 'statics',
            mainConfigFile: 'statics/js/main.js',
            name: 'js/main',
            out: 'statics/public/js/main.min.js',
            preserveLicenseComments: false,
            paths:{
                requireLib:'bower_components/requirejs/require'
            },
            include: 'requireLib'
        }
    }

};
