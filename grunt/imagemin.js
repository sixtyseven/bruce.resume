/**
 * Created by bruce on 15/08/2015.
 */
module.exports = {
    dist:{
        files: [{
            expand: true,
            cwd: 'statics',
            src: 'images/*.{png,jpg,gif}',
            dest: 'statics/public/'
        }]
    }
}