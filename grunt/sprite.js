/**
 * Created by bruce on 18/07/2015.
 */


module.exports = {
    icons:{
        src: './statics/images/sprites/icons/**/*.png',
        dest: './statics/images/icon-sprites.png',
        destCss: './statics/less/icon-sprites.less',
        padding: 2
    },
    portfolio:{
        src: './statics/images/sprites/portfolio/**/*.png',
        dest: './statics/images/portfolio-sprites.png',
        destCss: './statics/less/portfolio-sprites.less',
        padding: 2
    }
}