module.exports = {
    less:{
        files: [
                'statics/less/**/*.less'
                ],
        tasks: ['less']
    },
    sprite:{
        files: [
            'statics/images/sprites/**/*.png'
        ],
        tasks: ['sprite', 'less']
    }
};