module.exports = {
    dist:{
        options:{
          ignore: ['.ignore-place-holder'],
            urls: ['http://v.ubuntu.trusty:3000/']
        },
        files:{
            'statics/css/styles.min.css': ['views/index.html']
        }
    }
}