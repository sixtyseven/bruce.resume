module.exports = {
    my_target:{
        options: {
            paths: ['statics/css/']
        },
        files: {
            'statics/public/css/styles.min.css': ['statics/css/styles.css']
        }
    }
}