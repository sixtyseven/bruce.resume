module.exports = {
    development:{
        options: {
            paths: ['statics/']
        },
        files:{
            'statics/css/styles.css': ['statics/less/main.less']
        }
    }
}

