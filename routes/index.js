module.exports = function(dataSource){
    var express = require('express');
    var router = express.Router();
    var nodemailer = require('nodemailer');

    var env = process.env.NODE_ENV || 'development';
        //env = 'production';

    /* GET home page. */
    router.get('/', function(req, res) {
        var template = 'templates/index';
        var options = {
            data: dataSource.data,
            env: env
        };
        res.render(template, options);
    });

    /* send email to me */
    router.get('/contact_me', function(req, res) {
        var transporter = nodemailer.createTransport();

        transporter.sendMail({
            from: 'myprofile@bruceli.com',
            to: 'lsq991@gmail.com',
            subject: 'someone contact me from my profile website',
            text: JSON.stringify(req.query)
        });

        res.json({
           sent: true
        });
    });

    return router;
};

